#!/usr/bin/env python
# -*- coding: utf-8 -*-
import urllib
import simplejson as json
import pprint
import sys
import flask
import os
from flask import request, Flask


KEY = "&key=AIzaSyCUOpmzG5f0xP2A6udG1o5XiFLSuDW46gw"
types_map = { 1 : "restaurant", 2 : "school", 3 : "hospital", 4 : "shopping_mall", 5 : "police", 0 : "police|hospital|shopping_mall|school|restaurant"}
LATLONG = "12.912845,77.637923" #"12.912845,77.637923" 
print "Lat Long is : ", LATLONG 
RADIUS = "&radius=1500" 
#TYPES = "&types=book_store|hospital|atm|library|shopping_mall|school|restaurant"
TYPES = "&types=restaurant"
        
def location_name(lat, lang):
    LATLONG = lat + "," + lang #"12.912845,77.637923" 
    RADIUS = "&radius=1500" # + 1500
    TYPES = "&types=book_store|hospital|atm|library|shopping_mall|school|restaurant"
    LOCATION_NAME = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + LATLONG + KEY
    print LOCATION_NAME
    googleResponse = urllib.urlopen(LOCATION_NAME)
    jsonResponse = json.loads(googleResponse.read())
    location_name = None
    print jsonResponse['status']
    if jsonResponse['status'] != "OK":
        print "Not OK JSON response"
        return location_name
    for x in jsonResponse['results'][0]['address_components']:
        #location_name = ""
        if "sublocality_level_1" in x['types']:
            #print "Short Name : ", x['short_name']
            location_name = x['short_name']
        if "locality" in x['types']:
            #print "Locality : ", x['short_name']
            if not location_name:
                location_name = x['short_name'] 
        if "administrative_area_level_2" in x['types']:
            pass
            #location_name = location_name + x['short_name']
            #print "Administrative Name : ",x['short_name']
        #print location_name
    return location_name
    
def food_fulldata():
    food(True)
    
def food(full_data=False, filters=0):
    print int(filters)
    TYPES = "&types=" + types_map[int(filters)]
    print TYPES
    print "Lat Long is : ", LATLONG
    #LATLONG = sys.argv[1] #"12.912845,77.637923" 
    #RADIUS = "&radius=" + (sys.argv[2] if sys.argv[2] else "1500")
    URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + LATLONG + RADIUS + TYPES + KEY
    print "\nURL we are hitting is :", URL, "\n"
    googleResponse = urllib.urlopen(URL)
    jsonResponse = json.loads(googleResponse.read())
    json_new = json.dumps(jsonResponse)
    if not full_data:        
        for x in jsonResponse['results']:
            if "types" in x:
                rating = None
                if 'rating' in x:
                    rating = x['rating']
                #print x['name'], rating, x['types'][0], x['types'][1]
                json_new = "{ name : " + x['name'] + ", rating :" + str(rating) + ", type :" + str(x['types'][0]) + " }"
                print json_new.add
    return json_new
    #return json_new

def map_json_nextpage(nextpagetoken=None, new_json_test=None, full_data=False, filters=0, latlong="12.912845,77.637923"):
    #print nextpagetoken
    URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latlong + RADIUS + filters + "&pagetoken="+ nextpagetoken + KEY
    print "\nNext Page token URL we are hitting is :", URL, "\n"
    googleResponse = urllib.urlopen(URL)
    jsonResponse = json.loads(googleResponse.read())
    #json_new = json.dumps(jsonResponse)
    if jsonResponse['status'] == 'INVALID_REQUEST':
        json_test = map_json_nextpage(nextpagetoken, new_json_test, full_data, filters, latlong)
    json_test = new_json_test
    for x in jsonResponse['results']:
        if "types" in x:
            rating = None
            if 'rating' in x:
                rating = x['rating']
            #print x['name'], rating, x['types'][0], x['types'][1]
            print x['geometry']['location']['lat']
            #json_new = "{ name : " + x['name'] + ", rating :" + str(rating) + ", type :" + str(x['types'][0]) + " }"
            json_test.append({ "name" : x['name'], "rating" : str(rating), "type" : str(x['types'][0]), "lat" : x['geometry']['location']['lat'], "lang" : x['geometry']['location']['lng']}) 
            #print json_new
    return json_test
    
def map_json(full_data=False, filters=0, latlong="12.912845,77.637923"):
    TYPES = "&types=" + types_map[int(filters)]
    print TYPES
    LATLONG = latlong
    print "Lat Long is : ", LATLONG
    #LATLONG = sys.argv[1] #"12.912845,77.637923" 
    #RADIUS = "&radius=" + (sys.argv[2] if sys.argv[2] else "1500")
    URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + LATLONG + RADIUS + TYPES + KEY
    print "\nURL we are hitting is :", URL, "\n"
    googleResponse = urllib.urlopen(URL)
    jsonResponse = json.loads(googleResponse.read())
    json_new = json.dumps(jsonResponse)
    print json_new
    json_test = []
    print type(full_data)
    for x in jsonResponse['results']:
        if "types" in x:
            rating = None
            if 'rating' in x:
                rating = x['rating']
            #print x['name'], rating, x['types'][0], x['types'][1]
            print x['geometry']['location']['lat']
            json_new = "{ name : " + x['name'] + ", rating :" + str(rating) + ", type :" + str(x['types'][0]) + " }"
            json_test.append({ "name" : x['name'], "rating" : str(rating), "type" : str(x['types'][0]), "lat" : x['geometry']['location']['lat'], "lang" : x['geometry']['location']['lng']}) 
            print json_new
    if 'next_page_token' in jsonResponse:
        json_test = map_json_nextpage(jsonResponse['next_page_token'], json_test, full_data, TYPES, latlong)
    json_new = json.dumps(json_test, indent=4)
    json_test.append(place_type(json_test))
    return json_test
    #return json_new
    
def place_type(json_input):
    y = { 'restaurant': 0 , 'school': 0, 'shopping_mall': 0, 'hospital': 0, 'police': 0 }
    for x in json_input:
        if x['type'] in ('restaurant', 'cafe'):
            y['restaurant'] += 1
        elif x['type'] == 'school':
            y['school'] += 1
        elif x['type'] == 'shopping_mall':
            y['shopping_mall'] += 1
        elif x['type'] == 'hospital':
            y['hospital'] += 1
        elif x['type'] == 'police':
            y['police'] += 1
    print "Y is ", y
    return y

app = Flask(__name__)

@app.route('/')
def main_page():
    LATLONG = request.args.get('latlong')
    FULL_DATA = bool(str(request.args.get('full_data')))
    FILTER = request.args.get('filter')
    print "Filter " ,FILTER, LATLONG
    json_data_dump = map_json(full_data=FULL_DATA, filters=FILTER, latlong=LATLONG)
    for x in json_data_dump:
        pass #print x['type']
    return json.dumps(json_data_dump, indent=4)

@app.route('/json')
def call_food():
    return food()
    
@app.route('/food_full')
def call_food_full_data():
    return food(True)
    
@app.route('/api/<int:x>')
def filter_type(x):
    print request.args.get('latlong')
    return food(full_data=True, filters=x)

if __name__ == '__main__':
    import csv
    if len(sys.argv) > 1:
        LATLONG = sys.argv[1]
    port = int(os.environ.get("PORT",5000))
    host = "127.0.0.1" # '0.0.0.0'
    new_rows = []
    fieldnames = ['Lat', 'Lang', 'Location']
    f = open('workfile-new.csv', 'w+')
    with open('location.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            #temp = "{ 'Lat' :" + row['Lat'] + ", 'Lang' :" + row['Lang'] + ", 'Location' : 'test'" +"}"
            location = "" #location_name(str(row['Lat']), str(row['Lang']))
            if location:
                temp = str(row['Lat']) + ", " + str(row['Lang']) + ", " + location + "\n"
            #new_rows.append(temp)
            #f.write(temp.encode('utf-8'))
            #row['Location'] = location_name(str(row['Lat']), str(row['Lang']))
            #print row['Lat'], row['Lang']
    #print new_rows
    #with open('location_updated.csv', "wb") as csvfile:
    #    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        #writer.writeheader()
        #for row in new_rows:
    #    writer.writerows(new_rows)
            #print row['Lat'], row['Lang']
    '''
    f = open('location.csv', 'r')
    for line in f.readlines():
        print line
    '''
    app.run(host=host, port=port, debug = True)
